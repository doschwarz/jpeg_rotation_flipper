import os
import shutil
import piexif
import logging
from enum import Enum


############################################################
class PathTruncatingFormatter(logging.Formatter):
    def format(self, record):
        pathName = record.pathname
        if pathName is not None:
            dirName, fileName = os.path.split(pathName)
            truncPathName = os.path.join(*(dirName.split("\\")[-2:] + [fileName]))
            record.pathname = truncPathName
        
        return super(PathTruncatingFormatter, self).format(record)
        

############################################################
class ExifOrientation(Enum):
    TOP_LEFT = 1     # Landscape
    TOP_RIGHT = 2    # Landscape flipped
    BOTTOM_RIGHT = 3 # Landscape upside down
    BOTTOM_LEFT = 4  # Landscape upside down flipped 
    LEFT_TOP = 5     # Portrait shutter right flipped
    RIGHT_TOP = 6    # Portrait shutter right 
    RIGHT_BOTTOM = 7 # Portrait shutter left flipped
    LEFT_BOTTOM = 8  # Portrait shutter left


############################################################
PORTRAIT_ORIENTATIONS = {
    ExifOrientation.LEFT_TOP, ExifOrientation.RIGHT_TOP,
    ExifOrientation.RIGHT_BOTTOM, ExifOrientation.LEFT_BOTTOM
}
LANDSCAPE_ORIENTATIONS = {
    ExifOrientation.TOP_LEFT, ExifOrientation.TOP_RIGHT,
    ExifOrientation.BOTTOM_RIGHT, ExifOrientation.BOTTOM_LEFT
}


############################################################
def flip_portrait_orientation(exif):
    
    logger = logging.getLogger(__name__)
    
    exif_orientation_tag = 274
    exif_components_configuration_tag = 37121
    if exif_orientation_tag not in exif['0th']:
        return "no_orientation"
        
    orientation = ExifOrientation(exif['0th'][exif_orientation_tag])
    mapping = {ExifOrientation.RIGHT_TOP: ExifOrientation.LEFT_BOTTOM,
               ExifOrientation.LEFT_BOTTOM: ExifOrientation.RIGHT_TOP}
    
    if orientation in PORTRAIT_ORIENTATIONS:
        if orientation not in mapping:
            raise AssertionError("original orientation is {:d}, no flipping possible. Valid values: {}".format(orientation.value, {k.value for k in mapping}))
        exif['0th'][exif_orientation_tag] = mapping[orientation].value
        
        cc = exif['Exif'].get(exif_components_configuration_tag)
        if isinstance(cc, tuple):
            logger.debug("Changing exif components configuration from tuple to bytes")
            exif['Exif'][exif_components_configuration_tag] = ",".join([str(v) for v in cc]).encode("ASCII")
            
        return exif

    return None


############################################################
def flip_all_portrait_pictures(source_folder, target_folder=None):
    
    logger = logging.getLogger(__name__)
    jpg_extensions = {"JPG", "jpg", "JPEG", "jpeg"}
    
    assert os.path.isdir(source_folder)
    file_list = os.listdir(source_folder)
    assert len(os.listdir(source_folder)) > 0
    logger.info("Total number of files: {:d}".format(len(file_list)))
    
    if target_folder is not None:
        if not os.path.exists(target_folder):
            os.mkdir(target_folder)
        else:
            assert os.path.isdir(target_folder)
            if not len(os.listdir(target_folder)) == 0:
                logger.warning("target folder {} is not empty, files in it may be overwritten".format(target_folder))
        logger.info("Copying pictures to {}".format(target_folder))
    else:
        logger.info("Modifying pictures in {} in-place".format(source_folder))
        
    flipped_list = list()
    for counter, filename in enumerate(file_list):
        filepath = os.path.join(source_folder, filename)
        if not os.path.isfile(filepath):
            logger.debug("Not a file: {}".format(filepath))
            continue
        if filename.split(".")[-1] not in jpg_extensions:
            logger.debug("Not a jpeg: {}".format(filepath))
            continue
            
        exif = flip_portrait_orientation(piexif.load(filepath))
        
        if target_folder is not None:
            target_file = os.path.join(target_folder, filename)
            if os.path.exists(target_file):
                logger.debug("Overwriting {}".format(target_file))
                os.remove(target_file)
            shutil.copy2(filepath, target_file)
        else:
            target_file = filepath
            
        if exif is not None:
            if isinstance(exif, str):
                if exif == "no_orientation":
                    logger.info("Exif data of picture {} does not contain orientation information".format(filename))
                else:
                    raise RuntimeError("Invalid return string '{}'".format(exif))
            else:
                assert isinstance(exif, dict)
                try:
                    exif_bytes = piexif.dump(exif)
                except ValueError as e:
                    logger.warning("Exif data of picture {} is invalid. Error: {}".format(filename, e))
                    continue
                    
                piexif.insert(exif_bytes, target_file)
                logger.debug("flipped {}".format(filename))
                flipped_list.append(target_file)
               
        if counter % 100 == 0 and counter > 1:
            logger.info("Done with {:d} files".format(counter))
    
    logger.info("DONE. Flipped {:d} pictures".format(len(flipped_list)))
    
    return flipped_list
        
        
################################################################################
if __name__ == "__main__":
    
    import sys
    from argparse import ArgumentParser, RawTextHelpFormatter
    
    desc = "Python script that flips the orientation of all portrait oriented jpeg pictures in a folder"
    parser = ArgumentParser(description=desc, formatter_class=RawTextHelpFormatter)
    
    parser.add_argument("folder", type=str, help="path of folder that contains the pictures")
    parser.add_argument("--target-folder", type=str, help="if set, pictures will be copied to this folder,\nelse will be modified in-place")
    parser.add_argument("--debug", action="store_true", help="set logging level to DEBUG")
    
    args = parser.parse_args()
    
    logger = logging.getLogger()
    level = logging.DEBUG if args.debug else logging.INFO
    logger.setLevel(level)
    
    logging_handler = logging.StreamHandler(sys.stdout)
    logging_handler.setLevel(level)
    logging_handler.setFormatter(PathTruncatingFormatter("%(asctime)s [%(levelname)s] %(message)s [%(funcName)s %(pathname)s:%(lineno)d]"))
    
    logger.addHandler(logging_handler)
    
    flip_all_portrait_pictures(args.folder, args.target_folder)
